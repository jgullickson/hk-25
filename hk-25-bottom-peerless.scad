CLEARANCE = 1;
SPEAKER_SIZE = 83.75 + CLEARANCE;           // length and width of square speaker frame
SPEAKER_HEIGHT = 43.05;
FRAME_THICKNESS = 3.15;
CABINET_THICKNESS = 2;
CABINET_DIAMETER = sqrt((SPEAKER_SIZE * SPEAKER_SIZE) + (SPEAKER_SIZE * SPEAKER_SIZE));
CABINET_HEIGHT = CABINET_DIAMETER;
terminal_width = 10.7;
terminal_height = 10.1;
terminal_depth = 7.6;

difference(){
	union(){
		difference(){
            
			// cabinet
			cylinder(r=(CABINET_DIAMETER/2)+2,h=CABINET_HEIGHT,$fn=100);
			
			// center hole
			translate([0,0,1]){                
                cylinder(r=SPEAKER_SIZE/2,h=CABINET_HEIGHT,$fn=100);
            }
            
            translate([0,0,(CABINET_HEIGHT-SPEAKER_HEIGHT)]){
              #speaker();
            }
		}
        
        /*
		// terminal holder
		translate([(SPEAKER_SIZE/2)-(terminal_depth+2),-(terminal_width+8)/2,0]){
			cube([terminal_depth+2, terminal_width+8, terminal_height+3]);
		}
		*/
	}

    /*
	// terminal hole
	translate([(SPEAKER_SIZE/2)-terminal_depth,-(terminal_width+2)/2,-1]){
		#cube([terminal_depth+2, terminal_width+2, terminal_height+3]);
	}
	*/
}

module speaker(){

  // face bounding box
  translate([-SPEAKER_SIZE/2,-SPEAKER_SIZE/2,38.15]){   
    cube([SPEAKER_SIZE, SPEAKER_SIZE, 5]);
  }
  
  // magnet
  cylinder(r=(60/2), h=43.05);
  
}
