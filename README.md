# Printable HK-25 Replica

Print yourself a replica of the vintage Harman-Kardon HK-25.

# Status

I'm working on a redesign of this model around a specific speaker which is easier to find than the one I originally designed around.

Here's a link to the new speaker: https://www.parts-express.com/peerless-by-tymphany-tc9fd18-08-3-1-2-full-range-paper-cone-woofer--264-1062
